<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\MunusRepository;

use Menu;

class SiteController extends Controller
{
    //
	protected $p_rep; //portfolio repository
	protected $s_rep; //slider repository
	protected $a_rep; //articles repository
	protected $m_rep; //menus repository
	
	protected $template; //
	
	protected $vars = array(); // array variables
	
	protected $contentRightBar = FALSE; // 
	protected $contentLeftBar = FALSE; // 
	
	protected $bar = FALSE; // sidebar position
	
	//----------------------------
	public function __construct(MunusRepository $m_rep){
		$this->m_rep = $m_rep;
	}
	
	//----------------------------
	protected function renderOutput(){
		
		 $menu = $this->getMenu(); 
		 //dd($menu);
		
		$navigation = view(env('THEME').'.navigation')->render();
		$this->vars = array_add($this->vars,'navigation',$navigation);
		
		return view($this->template)->with($this->vars);
	}
	
	//----------------------------
	protected function getMenu() {
		
		$menu = $this->m_rep->get();
		
		Menu::make('MyNavBar', function ($menu) {
        $menu->add('Home');
        $menu->add('About', 'about');
        $menu->add('Services', 'services');
        $menu->add('Contact', 'contact');
    });
		
	  return $menu;
	}
	
	
}
